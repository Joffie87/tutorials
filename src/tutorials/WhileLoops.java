/*
 *         CCCCCCCCCCCCC                    ffffffffffffffff  FFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLL             
 *      CCC::::::::::::C                   f::::::::::::::::f F::::::::::::::::::::FL:::::::::L             
 *    CC:::::::::::::::C                  f::::::::::::::::::fF::::::::::::::::::::FL:::::::::L             
 *   C:::::CCCCCCCC::::C                  f::::::fffffff:::::fFF::::::FFFFFFFFF::::FLL:::::::LL             
 *  C:::::C       CCCCCC   ooooooooooo    f:::::f       ffffff  F:::::F       FFFFFF  L:::::L               
 * C:::::C               oo:::::::::::oo  f:::::f               F:::::F               L:::::L               
 * C:::::C              o:::::::::::::::of:::::::ffffff         F::::::FFFFFFFFFF     L:::::L               
 * C:::::C              o:::::ooooo:::::of::::::::::::f         F:::::::::::::::F     L:::::L               
 * C:::::C              o::::o     o::::of::::::::::::f         F:::::::::::::::F     L:::::L               
 * C:::::C              o::::o     o::::of:::::::ffffff         F::::::FFFFFFFFFF     L:::::L               
 * C:::::C              o::::o     o::::o f:::::f               F:::::F               L:::::L               
 * C:::::C       CCCCCCo::::o     o::::o f:::::f               F:::::F               L:::::L         LLLLLL
 *   C:::::CCCCCCCC::::Co:::::ooooo:::::of:::::::f            FF:::::::FF           LL:::::::LLLLLLLLL:::::L
 *    CC:::::::::::::::Co:::::::::::::::of:::::::f            F::::::::FF           L::::::::::::::::::::::L
 *      CCC::::::::::::C oo:::::::::::oo f:::::::f            F::::::::FF           L::::::::::::::::::::::L
 *         CCCCCCCCCCCCC   ooooooooooo   fffffffff            FFFFFFFFFFF           LLLLLLLLLLLLLLLLLLLLLLLL
 *                                 
 *                                        (Coalition of Friendly Learners)
 *                                       Joffie87, MaPeche91,& YellowZodiac
 */
package tutorials;
/**
 *
 * @author joffie87
 */
public class WhileLoops {
    public static String wloops() {
        System.out.println(" ");
        int value = 0;
        while(value < 10)        
        {
            System.out.println("Hello " + value);
                    value = value + 1;
        }
        String intv = String.valueOf(value);
        
                return
                     "Hello " + intv + "\n";                             
    }
    
}