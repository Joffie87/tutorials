/*
 *         CCCCCCCCCCCCC                    ffffffffffffffff  FFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLL             
 *      CCC::::::::::::C                   f::::::::::::::::f F::::::::::::::::::::FL:::::::::L             
 *    CC:::::::::::::::C                  f::::::::::::::::::fF::::::::::::::::::::FL:::::::::L             
 *   C:::::CCCCCCCC::::C                  f::::::fffffff:::::fFF::::::FFFFFFFFF::::FLL:::::::LL             
 *  C:::::C       CCCCCC   ooooooooooo    f:::::f       ffffff  F:::::F       FFFFFF  L:::::L               
 * C:::::C               oo:::::::::::oo  f:::::f               F:::::F               L:::::L               
 * C:::::C              o:::::::::::::::of:::::::ffffff         F::::::FFFFFFFFFF     L:::::L               
 * C:::::C              o:::::ooooo:::::of::::::::::::f         F:::::::::::::::F     L:::::L               
 * C:::::C              o::::o     o::::of::::::::::::f         F:::::::::::::::F     L:::::L               
 * C:::::C              o::::o     o::::of:::::::ffffff         F::::::FFFFFFFFFF     L:::::L               
 * C:::::C              o::::o     o::::o f:::::f               F:::::F               L:::::L               
 * C:::::C       CCCCCCo::::o     o::::o f:::::f               F:::::F               L:::::L         LLLLLL
 *   C:::::CCCCCCCC::::Co:::::ooooo:::::of:::::::f            FF:::::::FF           LL:::::::LLLLLLLLL:::::L
 *    CC:::::::::::::::Co:::::::::::::::of:::::::f            F::::::::FF           L::::::::::::::::::::::L
 *      CCC::::::::::::C oo:::::::::::oo f:::::::f            F::::::::FF           L::::::::::::::::::::::L
 *         CCCCCCCCCCCCC   ooooooooooo   fffffffff            FFFFFFFFFFF           LLLLLLLLLLLLLLLLLLLLLLLL
 *                                 
 *                                        (Coalition of Friendly Learners)
 *                                       Joffie87, MaPeche91,& YellowZodiac
 */
package tutorials;
import java.util.Scanner;
/**
 *
 * @author joffie87
 */
public class Main {
    boolean exit = false;
    public static void main(String[] args) {
        Main menu = new Main();
        menu.runMain();   
    } 
    public void runMain() {
        printHeader();
        while(!exit){
            printMenu();            
            int choice = getInput();
            performAction(choice);
    }
}
public void printHeader() {
    System.out.println("Joffie87 Tutorial Main Menu");
}   
public static void printMenu() {
    System.out.println(" ");
    System.out.println("1) Hello World!");
    System.out.println("2) Variables Tutorial");
    System.out.println("3) Strings");
    System.out.println("4) While Loops");
    System.out.println("0) Exit");
    System.out.println(" ");
    System.out.println("Input your choice number and hit enter.");
    System.out.println(" ");
}   
public int getInput() {
    Scanner kb = new Scanner(System.in);
    int choice = -1;
    while(choice < 0 || choice > 4 ){
        try {
            System.out.print("Enter your choice:");
            choice = Integer.parseInt(kb.nextLine());
            }
        catch (NumberFormatException e){
            System.out.println("Invalid Selection. Please try again.");
            }
        }
        return choice;
    }  
    private void performAction(int choice) {
        switch(choice) {
            case 0:
            exit = true;
            System.out.println("Thanks for checking out what I learned.");
            break;
            case 1:
            System.out.print(HelloWorld.hworld());
            break;                
            case 2:
            System.out.print(VariablesTutorial.vtut());
            break;
            case 3:
            System.out.print(Strings.stringstut());
            break;
            case 4:
            System.out.print(WhileLoops.wloops());
            break;
            default:
            System.out.println("An unknown error has occured.");
        }
    }
}