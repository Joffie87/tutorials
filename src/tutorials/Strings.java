/*
 *         CCCCCCCCCCCCC                    ffffffffffffffff  FFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLL             
 *      CCC::::::::::::C                   f::::::::::::::::f F::::::::::::::::::::FL:::::::::L             
 *    CC:::::::::::::::C                  f::::::::::::::::::fF::::::::::::::::::::FL:::::::::L             
 *   C:::::CCCCCCCC::::C                  f::::::fffffff:::::fFF::::::FFFFFFFFF::::FLL:::::::LL             
 *  C:::::C       CCCCCC   ooooooooooo    f:::::f       ffffff  F:::::F       FFFFFF  L:::::L               
 * C:::::C               oo:::::::::::oo  f:::::f               F:::::F               L:::::L               
 * C:::::C              o:::::::::::::::of:::::::ffffff         F::::::FFFFFFFFFF     L:::::L               
 * C:::::C              o:::::ooooo:::::of::::::::::::f         F:::::::::::::::F     L:::::L               
 * C:::::C              o::::o     o::::of::::::::::::f         F:::::::::::::::F     L:::::L               
 * C:::::C              o::::o     o::::of:::::::ffffff         F::::::FFFFFFFFFF     L:::::L               
 * C:::::C              o::::o     o::::o f:::::f               F:::::F               L:::::L               
 * C:::::C       CCCCCCo::::o     o::::o f:::::f               F:::::F               L:::::L         LLLLLL
 *   C:::::CCCCCCCC::::Co:::::ooooo:::::of:::::::f            FF:::::::FF           LL:::::::LLLLLLLLL:::::L
 *    CC:::::::::::::::Co:::::::::::::::of:::::::f            F::::::::FF           L::::::::::::::::::::::L
 *      CCC::::::::::::C oo:::::::::::oo f:::::::f            F::::::::FF           L::::::::::::::::::::::L
 *         CCCCCCCCCCCCC   ooooooooooo   fffffffff            FFFFFFFFFFF           LLLLLLLLLLLLLLLLLLLLLLLL
 *                                 
 *                                        (Coalition of Friendly Learners)
 *                                       Joffie87, MaPeche91,& YellowZodiac
 */
package tutorials;
/**
 *
 * @author joffie87
 */
public class Strings {
    public static String stringstut() {
        int myInt = 7;
        double myDouble = 7.8;
        String text = "Hello";
        String blank = " ";
        String name = "Bob";
        String greeting = text + blank + name;
    return
        "\n" +    
        greeting + "\n" + 
        "Hello Bob\n" + 
        name + blank + myInt + blank + text + "\n" +
        "My number is:" + myDouble + "." +
        "\n";
    }
}
