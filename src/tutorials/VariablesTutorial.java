/*
 *         CCCCCCCCCCCCC                    ffffffffffffffff  FFFFFFFFFFFFFFFFFFFFFFLLLLLLLLLLL             
 *      CCC::::::::::::C                   f::::::::::::::::f F::::::::::::::::::::FL:::::::::L             
 *    CC:::::::::::::::C                  f::::::::::::::::::fF::::::::::::::::::::FL:::::::::L             
 *   C:::::CCCCCCCC::::C                  f::::::fffffff:::::fFF::::::FFFFFFFFF::::FLL:::::::LL             
 *  C:::::C       CCCCCC   ooooooooooo    f:::::f       ffffff  F:::::F       FFFFFF  L:::::L               
 * C:::::C               oo:::::::::::oo  f:::::f               F:::::F               L:::::L               
 * C:::::C              o:::::::::::::::of:::::::ffffff         F::::::FFFFFFFFFF     L:::::L               
 * C:::::C              o:::::ooooo:::::of::::::::::::f         F:::::::::::::::F     L:::::L               
 * C:::::C              o::::o     o::::of::::::::::::f         F:::::::::::::::F     L:::::L               
 * C:::::C              o::::o     o::::of:::::::ffffff         F::::::FFFFFFFFFF     L:::::L               
 * C:::::C              o::::o     o::::o f:::::f               F:::::F               L:::::L               
 * C:::::C       CCCCCCo::::o     o::::o f:::::f               F:::::F               L:::::L         LLLLLL
 *   C:::::CCCCCCCC::::Co:::::ooooo:::::of:::::::f            FF:::::::FF           LL:::::::LLLLLLLLL:::::L
 *    CC:::::::::::::::Co:::::::::::::::of:::::::f            F::::::::FF           L::::::::::::::::::::::L
 *      CCC::::::::::::C oo:::::::::::oo f:::::::f            F::::::::FF           L::::::::::::::::::::::L
 *         CCCCCCCCCCCCC   ooooooooooo   fffffffff            FFFFFFFFFFF           LLLLLLLLLLLLLLLLLLLLLLLL
 *                                 
 *                                        (Coalition of Friendly Learners)
 *                                       Joffie87, MaPeche91,& YellowZodiac
 */
package tutorials;
/**
 *
 * @author joffie87
 */
public class VariablesTutorial {
    public static String vtut() {
        byte myByte = 13;  /**-128 to 127**/
        short myShort = 20145; /**-32,768 to 32,767**/       
        int myInt = 154687745; /**-2,147,483,648 to 2,147,483, 647**/        
        long myLong = 330146223; /**-9,223,372,036,854,775,808 to 
                                        9,223,372,036,854,775,807**/
        float myFloat = 3.1415926f; /**approximately ±3.40282347E+38F 
                                    (6-7 significant decimal digits) **/
        double myDouble = 3.14159265359; /**approximately ±1.79769313486231570E
                                         +308(15 significant decimal digits)**/        
        char myChar = 'X'; /** It is important to note that Java does not
                            support unsigned types. All int variants are signed.
                            But char is an exception; it is unsigned and 
                            consumes 2 bytes in memory. It stores 16-bit Unicode
                            UTF-16 character. Type char to be unsigned seems 
                            logical because there are no negative characters.**/
        char myChar2 = 'D';
        char myChar3 = 'ʘ' ;        
        boolean myBoolean = true; /**Can only be true or false. represents one
                                    bit of information, but its "size" isn't 
                                    something that's precisely defined.**/       
        String str1 = String.valueOf(myChar);
        String str2 = String.valueOf(myChar2);
        str1 = str1.concat(str2);                
        return
            " \n" +
            "'Byte' (" + myByte + ") -128 to 127\n" +
            "'Short'(" + myShort + ") -32,768 to 32,767\n" +
            "'Int' (" + myInt + ") -2,147,483,648 to 2,147,483, 647\n" +
            "'Long' (" + myLong + ") -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807\n" +
            "'Float' (" + myFloat + ") approximately ±3.40282347E+38F(6-7 significant decimal digits)\n" + 
            "'Double' (" + myDouble + ") approximately ±1.79769313486231570E+308(15 significant decimal digits)\n" +
            "\n" +
            "'Char'(" + myChar3 + ") It is important to note that Java does not\n" +
            "          support unsigned types. All int variants are signed.\n" +
            "          But char is an exception; it is unsigned and \n" +
            "          consumes 2 bytes in memory. It stores 16-bit Unicode\n" +
            "          UTF-16 character. Type char to be unsigned seems \n" +
            "          logical because there are no negative characters.\n" +
            "\n" +
            "'Chars converted to string' (" + str1 + ")\n" +
            "'Boolean' (" + myBoolean + ") Can only be true or false. represents one\n" +
            "                 bit of information, but its \"size\" isn't \n" +
            "                 something that's precisely defined. \n";                                
    }    
}